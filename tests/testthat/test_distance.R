context('distance')

test_that('calculating the distance matrix', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc)
  # ------------------- =
  
  dtm <- matrix(rnorm(5000), nrow = 500, ncol = 10)
  
  ev_it(argh1a <- CampaRi::distance(x = dtm, method = "euclidean_for", diag = T), NA)
  ev_it(argh1b <- CampaRi::distance(x = dtm, method = "euclidean", diag = T), NA)
  ev_it(argh1c <- CampaRi::distance(x = dtm, method = "euclidean_opt"), NA)
  ev_it(argh2 <- CampaRi::distance(x = dtm, method = "binary", diag = F), NA)
  ev_it(argh3 <- CampaRi::distance(x = dtm, method = "maximum", diag = F), NA)
  ev_it(argh4 <- CampaRi::distance(x = dtm, method = "manhattan", diag = F), NA)
  ev_it(argh5 <- CampaRi::distance(x = dtm, method = "canberra", diag = F), NA)
  ev_it(argh6 <- CampaRi::distance(x = dtm, method = "minkowski", diag = T, p = 3), NA)
  argh_comp1 <- as.matrix(dist(x = dtm, method = "euclidean", diag = T))
  argh_comp2 <- as.matrix(dist(x = dtm, method = "binary", diag = F))
  argh_comp3 <- as.matrix(dist(x = dtm, method = "maximum", diag = F))
  argh_comp4 <- as.matrix(dist(x = dtm, method = "manhattan", diag = F))
  argh_comp5 <- as.matrix(dist(x = dtm, method = "canberra", diag = F))
  argh_comp6 <- as.matrix(dist(x = dtm, method = "minkowski", diag = T, p = 3))
  expect_true(sum(abs(argh1a$d_out - argh_comp1)) < 0.000001) 
  expect_true(sum(abs(argh1b$d_out - argh_comp1)) < 0.000001) 
  # expect_true(sum(abs(argh1c$d_out - argh_comp1)) < 0.000001) # on the cluster it fails?
  expect_true(sum(abs(argh2$d_out - argh_comp2)) < 0.000001) 
  expect_true(sum(abs(argh3$d_out - argh_comp3)) < 0.000001) # W
  expect_true(sum(abs(argh4$d_out - argh_comp4)) < 0.000001) # W
  # expect_true(sum(abs(argh5$d_out - argh_comp5)) < 0.000001)  #??
  expect_true(sum(abs(argh6$d_out - argh_comp6)) < 0.000001) 
  # argh3$d_out[1:5,1:5]
  # argh_comp3[1:5,1:5]
  # argh4$d_out[1:5,1:5]
  # argh_comp4[1:5,1:5]
  
  # mutual information inspired by infotheo
  require(infotheo)
  data(USArrests)
  dat <- discretize(USArrests, nbins = 3)
  #computes the MIM (mutual information matrix)
  II  <- mutinformation(dat,method= "emp")
  I2 <- mutinformation(dat[,1],dat[,2])
  # I2
  mi.o <- CampaRi::distance(t(as.matrix(USArrests)), method = "mutual_info", nbins = 3)
  # mi.o$d_out
  # mi.o$d_out - II
  
  # ------------------------------------------------------------------------------------------------------- benchmarking
  if(F){
    # j
    asd <- microbenchmark::microbenchmark(
      "euclidean" = CampaRi::distance(x = dtm, method = "euclidean"),
      "euclidean_opt" = CampaRi::distance(x = dtm, method = "euclidean_opt"),
      "euclidean_R" = as.matrix(dist(x = dtm, method = "euclidean")),
      "maximum_R" = as.matrix(dist(x = dtm, method = "maximum")),
      "manhattan_R" = as.matrix(dist(x = dtm, method = "manhattan")),
      "minkowski_R" = as.matrix(dist(x = dtm, method = "minkowski", p = 3)),
      "canberra_R" = as.matrix(dist(x = dtm, method = "canberra")),
      "binary_R" = as.matrix(dist(x = dtm, method = "binary")),
      "euclidean_for" = CampaRi::distance(x = dtm, method = "euclidean_for"), 
      "euclidean_mag" = CampaRi::distance(x = dtm, method = "euclidean_mag"), 
      "maximum" = CampaRi::distance(x = dtm, method = "maximum"), 
      "manhattan" = CampaRi::distance(x = dtm, method = "manhattan"), 
      "canberra" = CampaRi::distance(x = dtm, method = "canberra"), 
      "binary" = CampaRi::distance(x = dtm, method = "binary"), 
      "minkowski" = CampaRi::distance(x = dtm, method = "minkowski", p = 3), 
      times = 10
    )
    # profvis::profvis(argh <- CampaRi::distance(x = dtm, method = "euclidean"))
    library(ggplot2)
    autoplot(asd)
  }
})

