---
title: "Feature selection (PCA)"
author: "Davide Garolini"
date: "4 Feb 2019"
vignette: >
  %\VignetteEngine{knitr::knitr}
  %\VignetteIndexEntry{feature_selectionPCA}
  %\usepackage[utf8]{inputenc}
---

```{r setup, include=FALSE}
if(!file.exists("to_d")) dir.create("to_d")
knitr::opts_chunk$set(echo = TRUE, eval = FALSE, warning = FALSE, results = TRUE, fig.width = 6, fig.height = 4.5)
# knitr::opts_knit$set(progress = FALSE)
knitr::opts_knit$set(root.dir="to_d/")
options(bitmapType="cairo")
```

## Feature selection

Using the default R PCA algorithms (or the enhanced ones which are slow (PCAproj)) this function select the features with the biggest loading. It is also possible to plot the result in 2 components.

```{r init_dataset, eval = TRUE}
library(mlbench); library(CampaRi)
data(PimaIndiansDiabetes)
# ?PimaIndiansDiabetes
# summary(PimaIndiansDiabetes)
# dim(PimaIndiansDiabetes)
slected_elements <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca') # automatically selected the first 2 components
# slected_elements_robust <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', pca_method = 'robust') # projection pursuit 2 components

# the selected_elements* objects are data_frames with 2 columns (selected variables)
# If I want to select more variables:
slected_elements <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', n_princ_comp = 4) 
```

## Including Plots

You can also generate plots, for example:

```{r, eval = TRUE}
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T)
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, 
                cluster_vector = sample(c(1,2,3), size = nrow(PimaIndiansDiabetes), replace = T))

# let's use a better clustering definition:
clu_vector <- PimaIndiansDiabetes[,9]
clu_vector <- as.factor(clu_vector)
levels(clu_vector) <- c(2,1)
clu_vector <- as.numeric(clu_vector)


select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, cluster_vector = clu_vector)

select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, frameit = T, cluster_vector = clu_vector)

plot1 <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, frameit = T, return_plot = T, cluster_vector = clu_vector)


# adding the legend?
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = T, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T)
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = F, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T, specific_palette = c("#b47b00", "#000000"))
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = F, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T, specific_palette = c("#b47b00", "#000000"))

select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = T, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T)

```



```{r outsetup, include = FALSE, eval = T}
system("rm ../to_d/*")
```


