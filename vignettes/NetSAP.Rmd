---
title: "NetSAP"
author: "Davide Garolini"
date: "14 Jan 2019"
vignette: >
  %\VignetteEngine{knitr::knitr}
  %\VignetteIndexEntry{NetSAP_vignette}
  %\usepackage[utf8]{inputenc}
---

```{r setup, include=FALSE}
.normalize <- function(x, xmax = NULL, xmin = NULL) { # not equivalent of scale(x, center = F)
  # if(.isSingleElement(x)) return(1)
  if(is.null(xmax)) xmax <- max(x)
  if(is.null(xmin)) xmin <- min(x)
  if(xmin == xmax) return(x/xmax)
  else return((x*1.0 - xmin)/(xmax - xmin))
}
if(!file.exists("to_d")) dir.create("to_d")
knitr::opts_chunk$set(echo = TRUE, eval = FALSE, warning = FALSE, results = TRUE, fig.width = 6, fig.height = 4.5)
# knitr::opts_knit$set(progress = FALSE)
knitr::opts_knit$set(root.dir="to_d/")
options(bitmapType="cairo")
```

This tutorial is focused on the application of NetSAP on a toy model. We build the model in the following way

## MST
We will use here the trajectory generated using the following command (needs original campari package):
```{r toy_model, eval = TRUE}
cat("STARTING VIGNETTE NetSAP_vignette...\n")
library(CampaRi); library(data.table)
# creating the oscillatory system - 3 states
tot_time <- 10000
ttt <- 1:tot_time
whn <- sample(ttt, size = 15)
wht <- sample(0:3, size = 16, replace = T)
tjum <- c(0, sort(whn), tot_time)
diff_tjum <- diff(tjum)
mat_fin <- array(0, dim = c(tot_time, 3))
annot <- array(NA, dim = tot_time)
for(i in 1:length(wht)){
  wherr <- (tjum[i]+1):tjum[i+1]
  if(wht[i]==1){
    mat_fin[wherr, 1] <- sin(wherr/5 + 0.7)
    mat_fin[wherr, 2] <- sin(wherr/5) + 0.2    
  } else if(wht[i]==2){
    mat_fin[wherr, 3] <- sin(wherr/5)
    mat_fin[wherr, 2] <- sin(wherr/5 + 0.5) + 0.2    
  } else if(wht[i]==3){
    mat_fin[wherr, 3] <- sin(wherr/5 + 0.35)
    mat_fin[wherr, 1] <- sin(wherr/5) + 0.2    
  }
  annot[wherr] <- rep(wht[i], diff_tjum[i]) 
}
mat_fin <- mat_fin + matrix(rnorm(tot_time*3)/5, nrow = tot_time, ncol = 3)
mat_fin <- .normalize(mat_fin)
plot_traces(t(mat_fin[250:500,]))
plot_traces(t(mat_fin))
plot(annot, pch = ".")
data.table::fwrite(as.data.frame(mat_fin), file = "test_trj.tsv", sep = " ")
```

Now we run a standard analysis to find the 4 clusters using the short spanning tree procedure to get the SAPPHIRE plot. Here no network inference is provided (to see all the passages, use `silent = F`).

```{r std_run, eval = TRUE}
SST_campari(trj = as.matrix(mat_fin), pi.start = 1, silent = T)
sapphire_plot("PI_output_basename_pistart1.dat", timeline = T, ann_trace = annot)
out_bar2 <- nSBR("PI_output_basename_pistart1.dat", ny = 100, n.cluster = 4, plot = T)
out_sc2 <- score_sapphire("PI_output_basename_pistart1.dat", ann = as.numeric(annot + 1), manual_barriers = out_bar2$barriers_sel, plot_pred_true_resume = T)
```

The NetSAP run, instead, should be able to unveil more clearly the involved states (to see all the passages, use `silent = F`).

```{r netsap_run, eval=T}
NetSAP(data_file = "test_trj.tsv", ncores_camp = 1, ncores_net = 1, folder = ".", trjsfile = "preproc_trj", base.name = "base_name",
       wii = 40, metho = "minkowski", princ_comps = 0, size_perc_search = 2000, qt = c(0.00015, 0.14),
       pi.start = 1, leaves.tofold = 5, search.attempts = 1000, state.width = 1000, print_mem = TRUE, silent = T)
sapphire_plot("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", timeline = T, ann_trace = annot)
out_bar <- nSBR("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", ny = 100, n.cluster = 4, plot = T)
out_sc1 <- score_sapphire("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", ann = as.numeric(annot + 1), manual_barriers = out_bar$barriers_sel, plot_pred_true_resume = T)
```


```{r outsetup, include = FALSE, eval = T}
system("rm ../to_d/*")
```
