#' @title Create a network for each snapshots 
#' @description
#'      \code{generate_network} creates a sequence of network (one for each snapshot) using a sliding window of trajectory (see \code{window}). 
#'      It is also possible to reduce the dimensionality using an overlapping reduction (see \code{overlapping_reduction})
#'
#' @param trj Input trajectory (variables on the columns and equal-time spaced snpashots on the row). It must be a \code{matrix} or a \code{data.frame} of numeric.
#' @param window Number of snapshots taken before and after the considered snapshot. 
#' @param overlapping_reduction Not yet supported (It will if a great number of snapshots will be considered in this analysis - snapshot selection)
#' @param pp_method 'path_euclidean', 'path_minkowski', 'path_manhattan', 'path_maximum', 'path_canberra', 'path_binary' will find the distance path to make
#' an hamiltonian cycle. 'svd' if a singular vector decomposition will be used to have the diagional of the d matrix as output. With 'SymmetricUncertainty' you will have
#' AdjacencySymmetricUncertainty=2*I(X;Y)/(H(X)+H(Y)) [method='MI_*' must be active]. 'tsne' will make a tsne transformation.
#' @param method Supported pairwise similarity measures are 'wgcna', 'binary', 'euclidean', 'maximum', 'canberra', 'minkowski', 'covariance', 
#' 'MI_MM'. If set 'none' the windows will be used only for the pp_method (works only with path_* or tsne/svd).
#' @param norm_method Normalization or standardization methods. If == 1 it will normilize the window. If == 2 it will standardize it. If 3 or 4 it will do the same
#' but on each variable of the window.
#' @param transpose_trj Defaults to F. If set T the junk of trj (i.e. with a specific window) is transposed so to infer a 
#' network with dimensions window*(window-1)/2
#' @param silent A logical value indicating whether the function has to remain silent or not. Default value is \code{FALSE}.
#' @param ... Various variables. Possible values are \code{c('wgcna_type', 'wgcna_power', 'wgcna_corOp')}. \code{use_decreasing_borders} is used to set the policy of the borders
#' 
#' @details From WGCNA::adjacency: Correlation and distance are transformed as follows: for type = "unsigned", 
#' adjacency = |cor|^power; for type = "signed", adjacency = (0.5 * (1+cor) )^power; for type = "signed hybrid",
#' adjacency = cor^power if cor>0 and 0 otherwise; and for type = "distance", adjacency = (1-(dist/max(dist))^2)^power.
#' For more details on the SAPPHIRE plot, please refer to the main documentation of the original 
#' campari software \url{http://campari.sourceforge.net/documentation.html}.
#'
#' @return It will return a modified trajectory matrix.
#' 
#' @seealso
#' \code{\link{adjl_from_progindex}}, \code{\link{gen_progindex}}, \code{\link{gen_annotation}}.
#' 
#' @examples
#' dt <- matrix(rnorm(10000), nrow = 1000, ncol = 10)
#' dt_net <- as.data.frame(generate_network(as.data.frame(dt), window = 5))
#' adjl <- mst_from_trj(trj = dt_net, dump_to_netcdf = F)
#' ret<-gen_progindex(adjl = adjl)
#' gen_annotation(ret_data = ret, local_cut_width = 10)
#' sapphire_plot("REPIX_000000000001.dat", ann_trace = sample(c(1,2,3,4,5), size = 1000, replace = T))
#' 
#' 
# @importFrom WGCNA adjacency
# @importFrom WGCNA mutualInfoAdjacency
# @importFrom WGCNA enableWGCNAThreads
# @importFrom WGCNA disableWGCNAThreads 
#' @importFrom data.table transpose
# @importFrom PairViz find_path
# @importFrom Rtsne Rtsne
# @importFrom TSA periodogram
#' @importFrom minerva mine
#' @importFrom parallel detectCores
#' 
#' 
#' @export generate_network

generate_network <- function(trj, window = NULL, method = 'wgcna', pp_method = NULL, norm_method = NULL,
                             overlapping_reduction = NULL, transpose_trj = FALSE, silent = FALSE, ...){
  
  # needed libraries
  nothing <- capture.output(.check_pkg_installation(c("WGCNA"))); rm(nothing) # just to rm the init prints
  
  # checking additional variable
  input_args <- list(...)
  
  avail_extra_argoments <- c('wgcna_type', 'wgcna_power', 'wgcna_corOp',                 # correlation options (WGCNA)
                             'minkowski_p',                                              # power for minkowski
                             'cov_method',                                               # cov met
                             'tsne_dimensions', 'tsne_perplexity', 'tsne_maxIter',       # tsne opts
                             'dbg_gn',                                                   # debugging
                             'do_multithreads',                                          # multithreading option
                             'use_decreasing_borders',                                   # policy for the borders. If F the initial windows will be identical
                             'multiplication_points')                                    # array of indeces. It indicates which points should be taken from each window! window_l will be added
  
  avail_methods <- c('none',                                                                                   # only post-proc
                     'wgcna',                                                                                  # cor
                     'binary', 'euclidean', 'manhattan', 'maximum', 'canberra', 'minkowski', #'mahalanobis',   # distances
                     'covariance',                                                                             # cov
                     'MI', 'MI_MM', 'MI_ML', 'MI_shrink', 'MI_SG',                                             # MI based
                     'MIC', 'MAS', 'MEV', 'MCN', 'MICR2', 'GMIC', 'TIC',                                       # mine based measures (minerva)
                     'fft',                                                                                    # fft 
                     "wgcna_emdl1",
                     "emdl1", 
                     "sig_emd",
                     "multiplication")                                                                                   
  
  which_are_distances <- c('binary', 'euclidean', 'manhattan', 'maximum', 'canberra', 'minkowski')
  MIC_mets <- c('MIC', 'MAS', 'MEV', 'MCN', 'MICR2', 'GMIC', 'TIC')
  wgcna_dep_mets <- c('wgcna', 'binary', 'euclidean', 'manhattan', 'maximum', 'canberra', 'minkowski', 'MI', 'MI_MM', 'MI_ML', 'MI_shrink', 'MI_SG')
  
  # default handling of extra inputs
  .checkExtraArgs(input_args, avail_extra_argoments, silent = silent)
  if(!('wgcna_type' %in% names(input_args))) wgcna_type <- 'unsigned' else wgcna_type <- input_args[['wgcna_type']]
  if(!('wgcna_power' %in% names(input_args))) wgcna_power <- 2 else wgcna_power <- input_args[['wgcna_power']]
  if(!('wgcna_corOp' %in% names(input_args))) wgcna_corOp <- "pearson" else wgcna_corOp <- input_args[['wgcna_corOp']]
  if(!('minkowski_p' %in% names(input_args))) minkowski_p <- 3 else minkowski_p <- input_args[['minkowski_p']]
  if(!('cov_method' %in% names(input_args))) cov_method <- 'pearson' else cov_method <- input_args[['cov_method']]
  if(!('tsne_dimensions' %in% names(input_args))) tsne_dimensions <- 2 else tsne_dimensions <- input_args[['tsne_dimensions']]
  if(!('tsne_perplexity' %in% names(input_args))) tsne_perplexity <- 30 else tsne_perplexity <- input_args[['tsne_perplexity']]
  if(!('tsne_maxIter' %in% names(input_args))) tsne_maxIter <- 500 else tsne_maxIter <- input_args[['tsne_maxIter']]
  if(!('dbg_gn' %in% names(input_args))) dbg_gn <- F else dbg_gn <- input_args[['dbg_gn']]
  if(!('do_multithreads' %in% names(input_args))) do_multithreads <- F else do_multithreads <- input_args[['do_multithreads']]
  if(!('use_decreasing_borders' %in% names(input_args))) use_decreasing_borders <- T else use_decreasing_borders <- input_args[['use_decreasing_borders']]
  if(!('multiplication_points' %in% names(input_args))) multiplication_points <- c(-1, -2, -3) else multiplication_points <- input_args[['multiplication_points']]
  
  # checks for fundamental variables: trj, window, method, (overlapping reduction)
  # trj checks ------------------------------------------------------------------------------------------------------------------------------
  
  # input trj checks
  if(is.data.frame(trj)){
    trj <- as.matrix(trj)
  }else if(is.character(trj)){
    if(!silent) cat("Reading trj file...\n")
    trj <- as.matrix(data.table::fread(file = trj, data.table = F)) 
  }else if(!is.matrix(trj)){
    stop("Unvalid trj insertion.")
  }
  dimtrj <- dim(trj)
  
  # long calculation warning
  if(dimtrj[1] > 20000) if(!silent) warning('The network generation can be really long. Please consider multi-threads options of the WGCNA package.')
  if(dimtrj[2] > 50) if(!silent) warning('The network generation can create an exagerated number of variables')
  
  # check for NA in trj
  if(any(is.na(trj))) stop('There are NA values in the input trajectory')
    
  # window size checks
  if(is.null(window)) window <- ceiling(dimtrj[1]/1000)
  
  .isSingleInteger(window, min = 3, max = dimtrj[1]/2)
  
  if(((window-1)/2)%%1 == 0) {
    window_r <- window_l <- (window-1)/2
  }else{
    window_r <- floor((window-1)/2)
    window_l <- ceiling((window-1)/2)
  }
  
  # norm_method checks
  if(!is.null(norm_method)) .isSingleInteger(norm_method, 1, 4)
  
  # border policy
  .isSingleLogical(use_decreasing_borders)
  
  # method checks
  if(!is.null(method) && (!is.character(method) || length(method) != 1)) stop('method must be a single character.')
  if(!(method %in% avail_methods)) stop(paste0('The method must be chosen between ', paste0(avail_methods, collapse = ' '),'.'))
  
  # multiplication method checks
  if(method == 'multiplication'){
    nothing <- sapply(multiplication_points, .isSingleInteger)
    multiplication_points <- multiplication_points + window_l
    if(min(multiplication_points) == 0) multiplication_points <- multiplication_points + 1
    if(max(multiplication_points) > window) multiplication_points <- multiplication_points - 1
    if(min(multiplication_points) < 1) stop("multiplication_points should be set according to the window. You inserted some negative values which is lower than half window.")
    if(max(multiplication_points) > window) stop("multiplication_points should be set according to the window. You inserted some positive values which is higher than half window.")
    if(use_decreasing_borders) {
      warning("Setting use_decreasing_borders = F because the multiplication selection could be problematic in the case of not same length windows which would happen at the borders.")
      use_decreasing_borders <- F
    }
  }
  if(method == "emdl1") .check_pkg_installation("earthmovdist")
  if(method == "wgcna_emdl1") .check_pkg_installation("earthmovdist")
  
  # checking MI method
  if(method == 'MI') method <- 'MI_MM'
  
  # enabling the multi-threading if using wgcna
  if(method %in% wgcna_dep_mets && do_multithreads) WGCNA::enableWGCNAThreads(nThreads = parallel::detectCores() - 1)
  if(method %in% MIC_mets && do_multithreads) ncores <- parallel::detectCores() - 1 else ncores <- 1
  
  # checks for logicals: transpose_trj, silent
  if(!is.logical(transpose_trj)) stop('transpose_trj must be a logical value.')
  if(!is.logical(silent)) stop('silent must be a logical value.')
  
  # overlapping reduction checks - not available
  if(!is.null(overlapping_reduction)) stop('overlapping_reduction functionality is not implemented still. Not use it.')
  # if((!is.null(overlapping_reduction) && (length(overlapping_reduction) != 1 ||!is.numeric(overlapping_reduction) ||
  #                                         overlapping_reduction <= 0 || overlapping_reduction > 1)))
  #   if(!silent) warning('The used overlapping_reduction is not correctly defined. It must be a number between 0 and 1.')
  
  # checks for available pp_method
  # pp_method checks ------------------------------------------------------------------------------------------------------------------------------
  avail_post_proc_met <- c('path_euclidean', 'path_minkowski', 'path_manhattan', 'path_maximum', 'path_canberra', 'path_binary', # find_path
                           'svd', 
                           'tsne',
                           'SymmetricUncertainty',
                           'amplitude', 'amplitude_maxfreq', 'maxfreq')
  
  # general post processing checks
  if(!is.null(pp_method)) .isSingleChar(pp_method)
  if(!is.null(pp_method) && !(pp_method %in% avail_post_proc_met)) 
    stop(paste0( "pp_method must be choosen between ", paste0(avail_post_proc_met, collapse = ' '))) 
  
  # post processing checks for MI
  if(!is.null(pp_method) && grepl(pp_method, pattern = 'SymmetricUncertainty', fixed = T) && !grepl(method, pattern = 'MI_', fixed = T)){
    if(!silent) warning('You did not insert any Mutual Information method (MI) but you are trying to use SymmetricUncertainty. The method will be set to "MI".')
    method <- 'MI_MM'
  }
  
  # post processing checks for fft
  if(!is.null(pp_method) && pp_method %in% c('amplitude', 'amplitude_maxfreq', 'maxfreq')){
    if(method != 'fft'){
      if(!silent) warning('You inserted pp_methods which depends on the fourier transformation of the dataset. "fft" method set automatically.')
      method <- 'fft'
      }
  }
  
  # checks if you inserted the pp_method when method is none (you do only post processing on windows)
  if(method == 'none' && is.null(pp_method)) stop('It is needed a pp_method if you want to use directly the window (without any network construction).')
  
  # checks for extra inputs 
  # extra input (...) checks ------------------------------------------------------------------------------------------------------------

  # wgcna specific inputs
  .isSingleInteger(wgcna_power)
  .isSingleChar(wgcna_type)
  .isSingleChar(wgcna_corOp)
  if(wgcna_corOp == 'pearson') wgcna_corOp <- "use = 'p'"
  else if(wgcna_corOp == 'spearman') wgcna_corOp <- "use = 'p', method = 'spearman'"
  else stop('Inserted wgcna_corOp not valid. choose between pearson and sperman.')
  
  # minkowski specific checks
  .isSingleInteger(minkowski_p)
  minkowski_p <- paste0(", p = '", minkowski_p, "'") 
  
  # specifiic checkings: the cov_method
  .isSingleChar(cov_method)
  if(!cov_method %in% c('pearson', 'spearman', 'kendall')) stop('cov_method not supported.')

  # specific checks for tsne
  if(!is.null(pp_method) && pp_method == 'tsne'){
    .isSingleInteger(tsne_dimensions)
    .isSingleInteger(tsne_perplexity)
    .isSingleInteger(tsne_maxIter)
    if(tsne_perplexity > (window - 1)/3){
      if(!silent) warning('tsne_perplexity too big. It will be set to tsne_perplexity <- (window/2 - 1)/3')
      tsne_perplexity <- (window/2 - 1)/3
    }  
  }
  # Checking the presence of specific packages for specific functionalities
  # package depencies ===============================================================================================
  if(method == "fft") .check_pkg_installation("TSA")
  if(!is.null(pp_method) && grepl(pp_method, pattern = 'emd', fixed = T)) .check_pkg_installation("earthmovdist")
  if(!is.null(pp_method) && pp_method == 'tsne') .check_pkg_installation("Rtsne")
  if(!is.null(pp_method) && grepl(pp_method, pattern = 'path_', fixed = T)) .check_pkg_installation("PairViz")
  
  # initialising the variables
  # __init ============================================================================================================
  
  # transpose option 
  # if(transpose_trj){
  #   trj_out <- matrix(NA, nrow = dimtrj[1], ncol = ((window*(window-1))/2)) 
  #   
  # # fft methods
  # }else if(method == 'fft'){
  #   if(!is.null(pp_method) && pp_method %in% c('amplitude', 'amplitude_maxfreq', 'maxfreq')){
  #     if(pp_method == 'amplitude')
  #       trj_out <- matrix(NA, nrow = dimtrj[1], ncol = dimtrj[2])
  #     else if(pp_method == 'maxfreq')
  #       trj_out <- matrix(NA, nrow = dimtrj[1], ncol = dimtrj[2])
  #     else if(pp_method == 'amplitude_maxfreq')
  #       trj_out <- matrix(NA, nrow = dimtrj[1], ncol = 2*dimtrj[2])
  #   }else{
  #     tester <- TSA::periodogram(trj[1:window, 1], plot = F)
  #     tester <- length(tester$spec)
  #     trj_out <- matrix(NA, nrow = dimtrj[1], ncol = tester*dimtrj[2])   
  #   }
  #   
  # # standard run methods (no post processing)
  # }else if(is.null(pp_method) || 
  #          (grepl(method, pattern = 'MI', fixed = T) && (!grepl(pp_method, pattern = 'path_', fixed = T) || !grepl(pp_method, pattern = 'svd', fixed = T)))){
  #   trj_out <- matrix(NA, nrow = dimtrj[1], ncol = ((dimtrj[2]-1)*dimtrj[2]/2)) 
  #   
  # # path construction for final matrix
  if(!is.null(pp_method) && grepl(pp_method, pattern = 'path_', fixed = T)){
    net_test <- WGCNA::adjacency(trj[1:100,])
    standard_path_length <- length(c(PairViz::find_path(net_test, path = function(xu) dist(xu, method = strsplit(pp_method, split = '_')[[1]][2]))))
    trj_out <- matrix(NA, nrow = dimtrj[1], ncol = standard_path_length)
  }
  # 
  # # final svd
  # }else if(!is.null(pp_method) && grepl(pp_method, pattern = 'svd', fixed = T)){
  #   trj_out <- matrix(NA, nrow = dimtrj[1], ncol = dimtrj[2])
  #   
  # # final tse
  # }else if(!is.null(pp_method) && grepl(pp_method, pattern = 'tsne', fixed = T)){
  #   trj_out <- matrix(NA, nrow = dimtrj[1], ncol = tsne_dimensions*window)
  # 
  # # other options are not available
  # }else{
  #   stop('impossible initilization of the variables. Please check the modes.')
  # }

  # time keeping variables
  if(dimtrj[1]*dimtrj[2] > 50000) timing_it <- TRUE
  else timing_it <- FALSE
  if(!silent && timing_it) time1 <- proc.time()
  
  
  if(dbg_gn) browser()
  # main transformation
  # __ loop ============================================================================================================
  
  # general loop
  if(!silent) cat('Network construction started (selected window: ', window, ').\n')
  # for(i in 1:dimtrj[1]){
  trj_out <- lapply(1:dimtrj[1], function(i){
    # adding time if necessary
    
    # print the bar if necessary
    if(!silent && timing_it) .print_consecutio(if(i==1) 0 else i, dimtrj[1], 70, timeit=timing_it, time_first = time1)
    
    # taking the piece of trj to transform
    if(use_decreasing_borders){
      if((i - window_l) <= 0) {                  # left border collision
        tmp_trj <- trj[1:(i + window_r),]
        if(transpose_trj || method == 'fft') tmp_trj <- rbind(tmp_trj, tmp_trj[1:(window - nrow(tmp_trj)),])
      }else if((window_r + i) > dimtrj[1]){      # right border collision
        tmp_trj <- trj[(i - window_l):dimtrj[1],]
        if(transpose_trj || method == 'fft') tmp_trj <- rbind(tmp_trj, tmp_trj[1:(window - nrow(tmp_trj)),])
      }else{                                     # usual center piece
        tmp_trj <- trj[(i - window_l):(i + window_r),]
      } 
    }else{
      if((i - window_l) <= 0) {                  # left border collision
        tmp_trj <- trj[1:(window_l + window_r),]
        if(transpose_trj || method == 'fft') tmp_trj <- rbind(tmp_trj, tmp_trj[1:(window - nrow(tmp_trj)),])
      }else if((window_r + i) > dimtrj[1]){      # right border collision
        tmp_trj <- trj[(dimtrj[1] - window_r - window_l):dimtrj[1],]
        if(transpose_trj || method == 'fft') tmp_trj <- rbind(tmp_trj, tmp_trj[1:(window - nrow(tmp_trj)),])
      }else{                                     # usual center piece
        tmp_trj <- trj[(i - window_l):(i + window_r),]
      } 
    }
    
    # a <- matrix(rnorm(1000, mean = 100), nrow = 100, ncol = 2); a[,2] <- a[,2] + rnorm(100, 100, 100)
    # c <- .normalize(a)
    # c <- (a - mean(a))/sd(a)
    # b <- apply(a, 2, .normalize)
    # b <- apply(a, 2, function(x) (x - mean(x))/sd(x))
    # ggplot(data = data.frame("a" = c[,1], "b" = c[,2])) + geom_histogram(aes(a), col = "red") + geom_histogram(aes(b), col = "blue")
    # ggplot(data = data.frame("a" = b[,1], "b" = b[,2])) + geom_histogram(aes(a), col = "red") + geom_histogram(aes(b), col = "blue")
    if(!is.null(norm_method)){
      if(norm_method == 1) tmp_trj <- .normalize(tmp_trj)
      if(norm_method == 2) tmp_trj <- (tmp_trj - mean(tmp_trj))/sd(tmp_trj)
      if(norm_method == 3) tmp_trj <- apply(tmp_trj, 2, .normalize)
      if(norm_method == 4) tmp_trj <- apply(tmp_trj, 2, function(x) (x - mean(x))/sd(x))
      if(anyNA(tmp_trj)) tmp_trj[is.na(tmp_trj)] <- min(tmp_trj, na.rm = T)
    }
    
    # transpose case
    if(transpose_trj) tmp_trj <- suppressWarnings(transpose(data.frame(tmp_trj))) # suppress the warnings for lost names
    
    # main adjacency constructor.
    # main --------------------------
    if(method != 'none'){
      
      # WGCNA
      if(method == 'wgcna'){
        built_net <- WGCNA::adjacency(tmp_trj, type = wgcna_type, corFnc = 'cor', power = wgcna_power, corOptions = wgcna_corOp)
      
      # covariance
      }else if(method == 'covariance'){
        built_net <- stats::cov(tmp_trj, method = cov_method)
      
      # mahalanobis (stat implementation) # todo
      # }else if(method == 'covariance'){
      #   built_net <- stats::cov(tmp_trj, method = cov_method)
      
      # MI based
      }else if(grepl(method, pattern = 'MI_', fixed = T)){
        while(dim(tmp_trj)[1] < 4) tmp_trj <- rbind(tmp_trj, tmp_trj)
        built_net <- WGCNA::mutualInfoAdjacency(tmp_trj, entropyEstimationMethod = strsplit(method, split = '_')[[1]][2])
      
      # construction based on Fourier Transform
      }else if(method == 'fft'){
        vec_freq <- c()
        for(fr in 1:dimtrj[2]){
          if(is.null(pp_method) || (!is.null(pp_method) && pp_method != 'amplitude')) # just to avoid further calc
            freq_spectr <- TSA::periodogram(tmp_trj[,fr], plot = F)
          if(!is.null(pp_method) && pp_method %in% c('amplitude', 'amplitude_maxfreq', 'maxfreq')){
            if(pp_method == 'amplitude')
              vec_freq <- c(vec_freq, diff(range(tmp_trj[,fr])))
            else if(pp_method == 'maxfreq')
              vec_freq <- c(vec_freq, freq_spectr$freq[which(freq_spectr$spec == max(freq_spectr$spec))])
            else if(pp_method == 'amplitude_maxfreq')
              vec_freq <- c(vec_freq, diff(range(tmp_trj[,fr])), freq_spectr$freq[which(freq_spectr$spec == max(freq_spectr$spec))[1]])
          }else{
            vec_freq <- c(vec_freq, freq_spectr$spec)
          }
        }
        built_net <- vec_freq
      # distance inverse (e.g. Minkowskij similarity)
      }else if(method %in% which_are_distances){
        distOptions_manual <- paste0("method = '", method, "'")
        if(method == 'minkowski') distOptions_manual <- paste0(distOptions_manual, minkowski_p)
        built_net <- WGCNA::adjacency(tmp_trj, type = "distance", distOptions = distOptions_manual)
      
      # eventual misunderstanding
      }else if(method %in% MIC_mets){
        built_net <- minerva::mine(x = tmp_trj, n.cores = ncores, alpha = 0.4, C = 2)
        if(method == 'MIC') built_net <- built_net$MIC
        else if(method == 'MAS') built_net <- built_net$MAS
        else if(method == 'MEV') built_net <- built_net$MEV
        else if(method == 'MCN') built_net <- built_net$MCN
        else if(method == 'MICR2') built_net <- built_net$MICR2
        else if(method == 'GMIC') built_net <- built_net$GMIC
        else if(method == 'TIC') built_net <- built_net$TIC
      }else if(method == "wgcna_emdl1"){
        built_net <- WGCNA::adjacency(tmp_trj, type = "distance", corFnc = earthmovdist::emdL1)
      }else if(method == "emdl1"){
        built_net <- .emd_m1(tmp_trj)
      }else if(method == "sig_emd"){
        built_net <- .emd_m2(tmp_trj)
      }else if(method == "multiplication"){
        built_net <- tmp_trj[multiplication_points,]
      }else{
        stop('Something in the method construction went wrong. Please refer to the developers.')
      }
    # none case -> pass on the post processing without modifications
    }else{
      built_net <- tmp_trj
    }
          
    # Post-processing
    # pp_method -------------------------
    # special case: MI
    if(grepl(method, pattern = 'MI_', fixed = T)){
      if(!is.null(pp_method) && grepl(pp_method, pattern = 'SymmetricUncertainty', fixed = T))
        built_net_fin <- built_net$AdjacencySymmetricUncertainty
      else
        built_net_fin <- built_net$MutualInformation
    }else if(method != 'fft'){
      built_net_fin <- built_net
    }else{
      built_net_fin <- built_net
    }
    # other cases    
    if(method == 'fft'){
      return(built_net_fin)
    }else if(!is.null(pp_method) && grepl(pp_method, pattern = 'path_', fixed = T)){
      tmp_path <- c(PairViz::find_path(built_net_fin, path = function(xu) dist(xu, method = strsplit(pp_method, split = '_')[[1]][2])))
      if(standard_path_length != length(tmp_path)) stop('the path length changed during the analysis. This is not possible.')
      return(tmp_path)
    }else if(!is.null(pp_method) && grepl(pp_method, pattern = 'svd', fixed = T)){
      return(svd(built_net_fin, nu = 0, nv = 0)$d)
    }else if(!is.null(pp_method) && grepl(pp_method, pattern = 'tsne', fixed = T)){
      tmp_to_expand <- c(Rtsne::Rtsne(X = built_net_fin, dims = tsne_dimensions, perplexity = tsne_perplexity, verbose = FALSE, max_iter = tsne_maxIter)$Y)
      return(tmp_to_expand)
    }else if(method == "multiplication"){
      return(c(built_net_fin))
    }else{
      # Taking only the upper.tri
      return(built_net_fin[lower.tri(x = built_net_fin, diag = FALSE)])
    }
  })
  # check for dimensions
  # res <- microbenchmark::microbenchmark("A" = matrix(unlist(trj_out, use.names = F), ncol = length(trj_out[[1]]), byrow = TRUE), 
  #                                       "B" = do.call(rbind,lapply(trj_out,matrix,ncol= length(trj_out[[1]]),byrow=TRUE)))
  # ggplot2::autoplot(res)
  trj_out <- matrix(unlist(trj_out, use.names = F), ncol = length(trj_out[[1]]), byrow = TRUE)
  dimtrjout <- dim(trj_out)
  if(dimtrjout[1] != dimtrj[1]) warning("sapply produced different dimensions from the inserted ones. In detail input dim: ", dimtrj[1], " ", dimtrj[2], ", output dim: ", dimtrjout[1], " ", dimtrjout[2])
  # __ end loop ============================================================================================================
  #
  if(method %in% wgcna_dep_mets && do_multithreads) WGCNA::disableWGCNAThreads()
  
  # Checking the creation of NAs in the inference
  # final output -------------------------
  n_na_trj <- sum(is.na(trj_out))
  if(n_na_trj > 0){
    if(!silent) warning('Attention: NA generated. Maybe too short window of time used? Fraction of NA: ', (n_na_trj*100/(dimtrjout[1]*dimtrjout[2])), ' %')
    if((n_na_trj*100/(dimtrjout[1]*dimtrjout[2])) > 5) 
      stop('ATTENTION!!! The generated NA overcame the threshold of 5%. Please consider alternative methods and vars.')
    if(!silent) cat('Assigning 0 to the generated NA.\n')
    trj_out[is.na(trj_out)] <- 0 
  }
  if(!silent) cat('Network construction completed.\n')
  invisible(list('trj_out' = trj_out, 'n_na_trj' = n_na_trj))
}

#' @title Parallel version of \code{generate_network}
#' @description
#'      \code{generate_network} creates a sequence of network (one for each snapshot) using a sliding window of trajectory (see \code{window}). 
#'      It is also possible to reduce the dimensionality using an overlapping reduction (see \code{overlapping_reduction})
#'
#' @param trj Input trajectory (variables on the columns and equal-time spaced snpashots on the row). It must be a \code{matrix} or a \code{data.frame} of numeric.
#' @param n_cores number of cores to use!
#' @param log_file log file to use. Remember that parallelising in R does not allow any print if not to file (log_file)
#' @param ... This variables will go directly to \code{\link{generate_network}}
#' 
#' @return It will return a modified trajectory matrix.
#' 
#' @seealso
#' \code{\link{adjl_from_progindex}}, \code{\link{gen_progindex}}, \code{\link{gen_annotation}}.
#' 
#' @import parallel
#' 
#' @export par_generate_network
#' 

par_generate_network <- function(trj, n_cores = parallel::detectCores(), window = NULL, log_file = NULL, ...){
  
  # input trj checks
  if(is.data.frame(trj)){
    trj <- as.matrix(trj)
  }else if(is.character(trj)){
    trj <- as.matrix(data.table::fread(file = trj, data.table = F)) 
  }else if(!is.matrix(trj)){
    stop("Unvalid trj insertion.")
  }
  dimtrj <- dim(trj)
  
  # number of cores check
  .isSingleInteger(n_cores)
  if(n_cores > parallel::detectCores() + 1) stop("Selected more cores than physical ones.")
  
  # window size checks
  if(is.null(window)) window <- ceiling(dimtrj[1]/1000)
  
  .isSingleInteger(window, min = 3, max = dimtrj[1]/2)
  
  if(((window-1)/2)%%1 == 0) {
    window_r <- window_l <- (window-1)/2
  }else{
    window_r <- floor((window-1)/2)
    window_l <- ceiling((window-1)/2)
  }
  
  # logfile check
  if(!is.null(log_file)) .isSingleChar(log_file)
  
  # finding indexes for running it in parallel
  divisor <- floor(dimtrj[1]/n_cores)
  ini_vec <- c(window_l + 1, divisor*1:(n_cores-1) + 1)
  fin_vec <- c(divisor*1:(n_cores-1), dimtrj[1] - window_r)
  
  if(is.null(log_file)) cl <- parallel::makeCluster(n_cores, type = "FORK")
  else cl <- parallel::makeCluster(n_cores, type = "FORK", outfile = log_file)
  
  list_trj_pieces <- lapply(X = 1:n_cores, FUN = function(xx){
    return(list("trj" = trj[(ini_vec[xx] - window_l):(fin_vec[xx] + window_r),], "index" = xx))
  })

  out_trj <- parallel::parLapply(cl = cl, X = list_trj_pieces, fun = function(trj_piece){
    ind <- trj_piece$index
    cat("Net-build started for piece number", ind, "of the trj...\n")
    trj_tmp <- CampaRi::generate_network(trj = trj_piece$trj, window = window, silent = T, ...)
    cat("Net-build finished for piece number", ind, "of the trj...\n")
    cat("Output dims of the", ind, "piece:", dim(trj_tmp$trj_out), "\n")
    if(ind == 1) cat(length(1:divisor), " asd\n")
    if(ind == n_cores) cat(length(window_l:(dimtrj[1] - ini_vec[ind] + window_l)), " asd\n")
    if(ind == 1) return(as.matrix(trj_tmp$trj_out[1:divisor,]))
    if(ind == n_cores) return(as.matrix(trj_tmp$trj_out[window_l:(dimtrj[1] - ini_vec[ind] + window_l),]))
    cat(length(window_l:(divisor + window_l - 1)), " asd\n")
    return(as.matrix(trj_tmp$trj_out[window_l:(divisor + window_l - 1),]))
  })
  parallel::stopCluster(cl)
  rm(list_trj_pieces)
  return(do.call("rbind", out_trj))
}

# HELP functions ------------------------------------------------------------------------------------------------------------

# emd code based on emdL1
.emd_m1 <- function(A) {
  tmp <- sapply(1:ncol(A), function(x) sapply(1:ncol(A), function(xx) earthmovdist::emdL1(A[,x], A[,xx], verbose = F)))
  1 - .normalize(tmp)
}

# emd based on signature emd
.emd_m2 <- function(A) {
  tmp <- sapply(1:ncol(A), function(x) sapply(1:ncol(A), function(xx) .sig_emd(A[,x], A[,xx])))
  1 -.normalize(tmp)
}
.sig_emd <- function(x, y){
  
  Q = length(x) # occurencies, signature hist
  R = length(y) # same length
  
  if(Q == 0 || R == 0) return(NA)
  
  if(Q < R) stop('First argument must be longer than or equally long as second.')
  
  x <- sort(x, method = "quick")
  y <- sort(y, method = "quick")
  
  # Use integers as weights since they are less prome to precision issues when subtracting
  w_x = R # = Q*R/Q
  w_y = Q # = Q*R/R
  
  emd = 0.
  q = 1
  r = 1
  
  while(q < Q){
    if(w_x <= w_y){
      cost <- w_x * abs(x[q] - y[r])
      w_y <- w_y - w_x
      w_x <- R
      q <- q + 1
    }else{
      cost <- w_y * abs(x[q] - y[r])
      w_x <- w_x - w_y
      w_y <- Q
      r <- r + 1
    }
    emd <- emd + cost
  }
  # Correct for the initial scaling to integer weights
  return(emd / (Q * R))
}
Rcpp::cppFunction('double sig_emdC (NumericVector x, NumericVector y) {
  int Q = x.size();
  int R = y.size();
  double w_x = (double) R;
  double w_y = (double) Q;
  int q = 1;
  int r = 1;
  double cost;
  double emd = 0;
  
  if(Q == 0 || R == 0) return NAN;
  
  std::sort(y.begin(), y.begin() + R);
  std::sort(x.begin(), x.begin() + Q);
  
  while(q < Q){
    if(w_x <= w_y){
      cost = w_x * (double) abs(x[q] - y[r]);
      w_y = w_y - w_x;
      w_x = (double)R;
      q = q + 1;
    }else{
      cost = w_y * (double) abs(x[q] - y[r]);
      w_x = w_x - w_y;
      w_y = (double)Q;
      r = r + 1;
    }
    emd = emd + cost;
  }
  return emd / ((double) Q * (double) R);

}')
# a <- rnorm(1000)
# b <- rnorm(1000)
# .sig_emd(a,b)
# sig_emdC(a,b)

#' 
#' #' @title  \code{generate_network} for list of spike times
#' #' @description Creates internal cross correlations.
#' #' 
#' #' @param spike_times_l List of spike times. Every element is a different channel.
#' #' @param window Size of the epoch.
#' #' @param overlap Degree of overlap. Its max is window
#' #' @param silent Logical. Set the verbosity.
#' #' @return It will return a modified trajectory matrix.
#' #' 
#' #' @export gen_net_from_spti
#' 
#' gen_net_from_spti <- function(spike_times_l, window, overlap = 0, silent = F){
#'   
#'   data_tmp <- preproc_tiles(data_list = spike_times_l, epoch_size = window, overlap = overlap)
#'   
#'   # Get data dimensions
#'   n_epochs <- dim(ii_spike_times)[1]
#'   n_channels <- dim(ii_spike_times)[2]
#'   epoch_index <- 1:n_epochs
#'   
#'   nan_count = 0.0
#'   
#'   lt_inds <- lower_tri_indeces2(n_channels, diags = T) # c1 will be the first column, c2 the second # I do it only once
#'   nrolt_inds <- nrow(lt_inds)
#'   
#'   # main loop on the 
#'   cl <- parallel::makeForkCluster(parallel::detectCores())
#'   distances <- parallel::parSapply(epoch_index, cl = cl, function(i){ # in parallel
#'     e1 = i 
#'     e2 = i
#'     
#'     # Compute distances for all xcorr pairs between the two epochs
#'     xcorr_distances = array(NA, dim = c(round(n_channels * (n_channels - 1) / 2)))
#'     
#'     n_xcorr_distances = 0
#'     i_xcorr_distance = 0
#'     
#'     # vectorizing the two loops
#'     xcorr_distances <- sapply(1:nrolt_inds, function(xx){
#'       c1 <- lt_inds[xx, 1]
#'       c2 <- lt_inds[xx, 2]
#'       # i_xcorr_distance = i_xcorr_distance + 1
#'       
#'       # Only compute the xcorrs and distance in case there is a spike in all relevant channels
#'       if((ii_spike_times[e1,c1,2] - ii_spike_times[e1,c1,1]) > 0  && (ii_spike_times[e1,c2,2] - ii_spike_times[e1,c2,1]) > 0
#'          && (ii_spike_times[e2,c1,2] - ii_spike_times[e2,c1,1]) > 0 && (ii_spike_times[e2,c2,2] - ii_spike_times[e2,c2,1]) > 0){
#'         
#'         # Compute the xcorrs
#'         xcorr1 = xcorr_list_fast(spike_times[ii_spike_times[e1,c1,1]:ii_spike_times[e1,c1,2]], spike_times[ii_spike_times[e1,c2,1]:ii_spike_times[e1,c2,2]])
#'         xcorr2 = xcorr_list_fast(spike_times[ii_spike_times[e2,c1,1]:ii_spike_times[e2,c1,2]], spike_times[ii_spike_times[e2,c2,1]:ii_spike_times[e2,c2,2]])
#'         
#'         # EMD
#'         if(length(xcorr1) >= length(xcorr2)) return(signature_emd(xcorr1, xcorr2))
#'         else return(signature_emd(xcorr2, xcorr1))
#'       }else{
#'         return(NA)
#'       }
#'     })
#'     return(xcorr_distances)
#'   })
#'   browser()
#'   parallel::stopCluster(cl)
#'   ddd <- unlist(purrr::modify_depth(distances, 1, ~.x$d), use.names = F)
#'   nan_count <- sum(unlist(purrr::modify_depth(distances, 1, ~.x$n_nas), use.names = F))
#'   percent_nan = nan_count * 100.0 / ((n_channels * (n_channels - 1) / 2) * n_epoch_index_pairs)
#'   
#'   return(list("distances" = matrix(ddd, nrow = n_epochs, ncol = n_epochs), "percent_nan" = percent_nan)) # it is symmetric so it is not necessary
#' }
#' xcorr_list_fast <- function(in1, in2){
#'   "List of all time delays from a full cross correlation of the two inputs
#'   Parameters
#'   ----------
#'   in1 : numpy.ndarray
#'   Occurence times / indices
#'   in2 : numpy.ndarray
#'   Occurence times / indices
#'   "
#'   n1 = length(in1)
#'   n2 = length(in2)
#'   
#'   expa_n1n2 <- expand.grid.faster2(1:n2, 1:n1)
#'   C <- in2[expa_n1n2[, 1]] - in1[expa_n1n2[, 2]]
#'   
#'   return(C)
#' }
#' signature_emd <- function(x, y){
#'   "A fast implementation of the EMD on sparse 1D signatures like described in:
#'   Grossberger, L., Battaglia, FP. and Vinck, M. (2018). Unsupervised clustering
#'   of temporal patterns in high-dimensional neuronal ensembles using a novel
#'   dissimilarity measure.
#'   Parameters
#'   ----------
#'   x : numpy.ndarray
#'   List of occurrences / a histogram signature
#'   Note: Needs to be non-empty and longer or equally long as y
#'   y : numpy.ndarray
#'   List of occurrences / a histogram signature
#'   Notes: Needs to be non-empty and shorter or equally long as x
#'   Returns
#'   -------
#'   Earth Mover's Distances between the two signatures / occurrence lists
#'   "
#'   
#'   Q = length(x)
#'   R = length(y)
#'   
#'   if(Q == 0 || R == 0) return(NA)
#'   
#'   if(Q < R) stop('First argument must be longer than or equally long as second.')
#'   
#'   x <- sort(x, method = "quick")
#'   y <- sort(y, method = "quick")
#'   
#'   # Use integers as weights since they are less prome to precision issues when subtracting
#'   w_x = R # = Q*R/Q
#'   w_y = Q # = Q*R/R
#'   
#'   emd = 0.
#'   q = 1
#'   r = 1
#'   
#'   while(q < Q){
#'     if(w_x <= w_y){
#'       cost <- w_x * abs(x[q] - y[r])
#'       w_y <- w_y - w_x
#'       w_x <- R
#'       q <- q + 1
#'     }else{
#'       cost <- w_y * abs(x[q] - y[r])
#'       w_x <- w_x - w_y
#'       w_y <- Q
#'       r <- r + 1
#'     }
#'     emd <- emd + cost
#'   }
#'   # Correct for the initial scaling to integer weights
#'   return(emd / (Q * R))
#' }
#' # In this case we want to assume the epoch size not known and format the data accordingly
#' # This first implementation forces the tiling
#' preproc_tiles <- function(data_list, epoch_size, overlap = 0){
#'   # data_list should be a list of n_channels lists of spike times.
#'   total_max <- max(sapply(data_list, max))
#'   stopifnot(overlap >= 0, overlap < epoch_size)
#'   stopifnot(epoch_size*2 < total_max) # there must be at least two "tiles" in the data_list
#'   init_p <- seq(1, total_max, epoch_size - overlap)
#'   end_p <- init_p + epoch_size - 1
#'   epoch_tot2 <- list()
#'   for(i in 1:length(init_p)){
#'     epoch_tot2[[i]] <- lapply(1:length(data_list), function(x) {
#'       vec_tf <- data_list[[x]] >= init_p[i] & data_list[[x]] <= end_p[i]
#'       if(sum(vec_tf) > 0) return(data_list[[x]][vec_tf])
#'       else return(NA)
#'     })
#'   }
#'   tmp2 <- get_spikes_shapes(epoch_tot = epoch_tot2, e_siz = epoch_size)
#'   return(list("epoch_tot" = epoch_tot2, "sp_t" = tmp2$sp_t, "ii_sp_t" = tmp2$ii_sp_t))
#' }
