module mod_distance
!
! interface
!   function euclidean_d ( veci, vecj ) bind ( C, name='euclidean_d_c' )
!     use, intrinsic :: iso_c_binding
!     real ( c_double ) :: veci(*)
!     real ( c_double ) :: vecj(*)
!     real ( c_double ) :: euclidean_d
!     ! integer ( c_int ), VALUE :: nr ! nrow(x)
!     ! integer ( c_int ), VALUE :: nc ! ncol(x)
!     ! integer ( c_int ), value :: i1
!     ! integer ( c_int ), value :: i2
!   end function euclidean_d
! end interface

contains
  subroutine distance(val2, veci, vecj)
    use mod_variables_gen
    use mod_clustering
  ! CALLED from:
  ! vereal(8) it%sums(1:n_xyz,1)/(1.0*it%nmbrs)
  ! call distance(val,vec1,trj_data(1:n_xyz,i)) !val=tmp_d,it=scluster(j),i=i
  ! As i is the snapshot index could be that trj_data is taking the 1:n_xyz sized array from
  ! the big data-set trj_data            : data extracted for clustering (all read into memory -> large)

    implicit none

    real(8), INTENT(IN) :: veci(n_xyz), vecj(n_xyz)
    real(8), INTENT(OUT) :: val2
    real(8) vec_ref(n_xyz)
    real(8)  hlp, hlp2
    integer k
    real(8) :: vec_temp(n_xyz,2)

    vec_ref = 1
    val2 = 0.0
    hlp = 0.0
    hlp2 = 0.0

    if (dis_method .eq. 1) then
      do k=1,n_xyz
        hlp = abs(veci(k) - vecj(k))
        if (hlp.gt.180.0) hlp = abs(hlp - 360.0)
        val2 = val2 + hlp*hlp
      end do
      val2 = sqrt(val2/(1.0*n_xyz))
    else if(dis_method .eq. 5) then
      hlp = sum((veci(1:n_xyz) - vecj(1:n_xyz))**2)
      val2 = sqrt((1.0 * hlp)/(1.0 * (n_xyz))) ! why *3????? number of coo
    ! else if (dis_method .eq. 11) then
    !   hlp = ACOS(dot_product(vec_ref,veci)/&
    !   (dot_product(vec_ref,vec_ref) + &
    !   dot_product(veci,veci)))
    !   hlp2 = ACOS(dot_product(vec_ref,vecj)/&
    !   (dot_product(vec_ref,vec_ref) + &
    !   dot_product(vecj,vecj)))
    !   val2 = hlp2 - hlp
    else if(dis_method .eq. 12) then
      ! this method is an inner product simply. It is better to use the eucl?
      vec_temp(1:n_xyz,1) = veci(1:n_xyz) - vecj(1:n_xyz)
      vec_temp(1:n_xyz,2) = matmul(distance_mat, vec_temp(1:n_xyz,1))
      hlp = dot_product(veci(1:n_xyz) - vecj(1:n_xyz), vec_temp(1:n_xyz,2))
      val2 = sqrt(hlp/(1.0 * n_xyz))
    end if
  end



end module mod_distance



! A note on how R-core function (super fast) is doing the distance matrix
! ==============================================================================
!  i, j = looper indexes
!  ij = output index (vectorization is appening)
!  nr = nrows
!  nc = ncols
! ------------------------------------------------------------------------------
!  dc = (*diag) ? 0 : 1; /* diag=1:  we do the diagonal */
!  ij = 0;
!  for(j = 0 ; j <= *nr ; j++)
!      for(i = j+dc ; i < *nr ; i++)
!          d[ij++] = (*method != MINKOWSKI) ?
!               R_euclidean(x, *nr, *nc, i, j) :
!               R_minkowski(x, *nr, *nc, i, j, *p);
!
